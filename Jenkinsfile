/* -----------------------------------------------------------------------------------------------*/
/*                                         GLOBAL VARIABLE                                        */
/* -----------------------------------------------------------------------------------------------*/


def szGitUrlJenkins = "https://gitlab.com/oherrou/jenkinsexample.git"

/* -----------------------------------------------------------------------------------------------*/
/*                                          JOB FUNCTIONS                                         */
/* -----------------------------------------------------------------------------------------------*/

/* 
This job will clone the Jenkins Repository
\param szGitUrlJenkins: A parameter for this function, not used yet (string)
*/
def job_CloneJenkins(String szGitUrl)
{
  try
  {
    println("Clone the jenkins repository, for now it's not available")
    //git(url: szGitUrl)
  }
  catch (Exception e)
  {
    println(e)
    error("Exception")
  }  
}

/* 
This job will compile a specific PLAN
\param szPlan: the PLAN to compile use one of the following (string)
 - Xxxxx
 - Xxxxx
 - TODO describes the PLAN name
\param szParam: A parameter for this function, not used yet (string)
*/
def job_CompilePlan(String szPlan, String szBat)
{
  def iStatusBuild = 0

  try
  {    
  println("TODO: compile plan")
    //iStatusBuild = bat(returnStatus: true, script: szBat)
  }
  catch (Exception e)
  {
    println(e)
    iStatusBuild = 1
    error("Exception")
    slackNotifyBuildFailed("Build")
  }

  // Check output status
  if (iStatusBuild == 0) // Success
  {
    // Success, do nothing
  }
  else if (iStatusBuild == 1) // Erros
  {
    println("Build failed due to errors in the job [BUILD]")
    error("")
  }
  else if (iStatusBuild == 2) // Success with warnings 
  {
    currentBuild.result = 'UNSTABLE'
  }
}

/* 
This job will run coding rules verification
\param szParam: A parameter for this function, not used yet (string)
*/
def job_CodingRuleVerification(String szParam)
{
  println("TODO: Running coding rules verification")
}

/* 
This job will run cppcheck
\param szParam: A parameter for this function, not used yet (string)
*/
def job_CppCheck(String szParam)
{
  println("TODO: Running cppcheck")
}

/* 
This job will run doxygen
\param szParam: A parameter for this function, not used yet (string)
*/
def job_Doxygen(String szParam)
{
  println("TODO: Running doxygen")
}

/* 
This job will create a changelog
\param szParam: A parameter for this function, not used yet (string)
*/
def job_CreateChangelog(String szParam)
{
  println("TODO: Creating changelog")
}

/* 
This job will send a mail notification to the SoftwareTeam/Author
\param szParam: A parameter for this function, not used yet (string)
*/
def job_MailNotification(String szParam)
{
  println("TODO: Sending mail Notification")
}

/* 
This job will handle the delivery of the software and attachments
\param szParam: A parameter for this function, not used yet (string)
*/
def job_Delivery(String szParam)
{
  println("TODO: Delivering software")
}

/* 
This job will create jenkins artifact
\param szParam: A parameter for this function, not used yet (string)
*/
def job_Artifacts(String szParam)
{
  println("TODO: Creating Artifacts")
}

/* 
This job will clean the jenkins workspace
\param szParam: A parameter for this function, not used yet (string)
*/
def job_Clean(String szParam)
{
  println("TODO: Cleaning workspace")
}

/* -----------------------------------------------------------------------------------------------*/
/*                                            PIPELINE                                            */
/* -----------------------------------------------------------------------------------------------*/

/* Pipeline description
  Use the pipeline configuration file provided with this jenkinsFile if you're not familiar with 
  JenkinsFile
*/
pipeline 
{
  agent any

  options
  {    
    /* Pipeline timeout
     If the pipeline build has run more than the desired value, it will be shown as fail
     This is usefull if the pipeline require a manual input from the developer in case he forgets it
    */
    timeout(time: 20, unit: 'MINUTES')

    /* Disable concurrent Builds,
     The same project won't be able to be run in parallel, this will avoid any issue with shared ressources
     */
    disableConcurrentBuilds()
  }

  /* Stages description */
  stages
  {

    /* STAGE 1: Git Clone the Jenkins repository */
    stage ('Clone Jenkins Repository')
    {
      steps
      {
        script
        {
          job_CloneJenkins(szGitUrlJenkins)
        }
      }
    }

    /* STAGE 2: Compilation */
    stage ('Compilation')
    {
      /* Using parallel stages
      TODO: Check if IAR building and makefiles can be done in parallel, might need to lock some ressources
      */
      parallel
      {
        /* STAGE 2.1: Plan A */
        stage('Building Plan A')
        {
          steps
          {
            script
            {
              job_CompilePlan("PLAN A", "JenkinsScript/build.bat")
            }
          }
        }

        /* STAGE 2.2: Plan B */
        stage('Building Plan B')
        {
          steps
          {
            script
            {
              job_CompilePlan("PLAN B", "Dummy Param")
            }
          }
        }

        /* STAGE 2.3: Plan C */
        stage('Building Plan C')
        {
          steps
          {
            script
            {
              job_CompilePlan("PLAN C", "Dummy Param")
            }
          }
        }

      }
    }

    /* STAGE 3: Analyze */
    stage ("Analyse")
    {
      /* Using parallel stages */
      parallel
      {
        /* STAGE 3.1: Coding Rules */
        stage('Coding Rules')
        {
          steps
          {
            script
            {
             job_CodingRuleVerification("Dummy Param")
            }
          }
        }

        /* STAGE 3.2: CppCheck */
        stage('CppCheck')
        {
          steps
          {
            script
            {
             job_CppCheck("Dummy Param")
            }
          }
        }

        /* STAGE 3.3: Doxygen */
        stage('Doxygen')
        {
          steps
          {
            script
            {
             job_Doxygen("Dummy Param")
            }
          }
        }
      }
    }
    
    /* STAGE 4: Reporting */    
    stage ('Reporting')
    {
      /* Using parallel stages */
      parallel
      {
        /* STAGE 4.1: Changelog */
        stage('Changelog')
        {
          steps
          {
            script
            {
             job_CreateChangelog("Dummy Param")
            }
          }
        }
      }
    }

    /* STAGE 5: Notification */
    stage ('Notification')
    {
      steps
      {
        script
        {
          job_MailNotification("Dummy Param")
        }
      }
    }

    /* STAGE 6: Go for production */
    stage ('Go For Production ?')
    {
      steps
      {
        script
        {
          println("Go for production ?")
        }
        timeout(time: 10, unit: 'MINUTES')
        {
          input(message: 'Deploy to Production?', id: "Deployment") 
        }       
      }
    }

    /* STAGE 7: Delivery */
    stage ('Delivery')
    {
      steps
      {
        script
        {
          job_Delivery("Dummy Param")
        }
      }
    }

    /* STAGE 8: Clean & Artifacts */
    stage ('Clean & Artifacts')
    {
      steps
      {
        script
        {
          job_Artifacts("Dummy Param")
          job_Clean("Dummy Param")
        }
      }
    }
  }
}