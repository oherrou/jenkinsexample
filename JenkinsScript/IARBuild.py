__author__ = "Delta Dore"
__license__ = "GPL"
__version__ = "1.0.0"
__maintainer__ = "Olivier Herrou"
__email__ = "oherrou.ext@deltadore.com"
__status__ = "Production"

"""  IAR Build libary
  This library has been developed to build an IAR project with jenkins 

  Tested with the following python version:
   - Windows 7 32bits, Python V3.6.4
"""

"""----------------------------------------------------------------------------------------------"""
"""------------------------------------------- IMPORT -------------------------------------------"""
"""----------------------------------------------------------------------------------------------"""
import sys
import subprocess
import re
import os
import datetime
import argparse
import configparser

sys.path.append('pythontools')
sys.path.append('C:\\Program Files (x86)\\Jenkins\\workspace\\JenkinsExample_master\\pythontools\\CustomPrint')
print(sys.path)
import CustomPrint as cp



"""----------------------------------------------------------------------------------------------"""
"""------------------------------------------- GLOBAL -------------------------------------------"""
"""----------------------------------------------------------------------------------------------"""
gszInitialWorkingDir  = '' #Working directory from where this python script has been called
gszIarBuilderPath     = ''
gszIarCompilerVersion = ''
gszIarProjectPath     = ''
gszIarConfiguration   = ''
gszIarSkipJob         = ''
gszIarBuildLog        = 'IarBuildResultTmp.txt'
bIncreaseVerbosity    = False
gszIarDebugLocalPath  = ''

"""----------------------------------------------------------------------------------------------"""
"""------------------------------------------ FUNCTIONS -----------------------------------------"""
"""----------------------------------------------------------------------------------------------"""
def IARBuild_ParsedOutput(BuildOuput):
  iStart  = 0 # Start index for parsing string
  iEnd    = 0 # Stop index for parsing string
  iIndex  = 0 # Index for parsing string
  lWorkingList = [] # List of usefull data
  dWorkingDict = {} # Dictionnary of usefull associated for each .c and .s files

  with open(BuildOuput, 'r') as fiInput:
    for szLine in fiInput:
      print(szLine)
      if szLine.strip() == '':
        continue
      elif '\\' in szLine and not 'Error' in szLine and not 'Warning' in szLine:
        continue
      elif 'IAR ANSI' in szLine:
        continue
      elif 'Copyright' in szLine:
        continue
      elif 'Standalone' in szLine:
        continue
      elif 'Errors' in szLine:
        continue
      elif 'Warnings' in szLine:
        continue
      elif 'Total number of' in szLine:
        continue
      elif 'Failed to open file' in szLine and '\\' in szLine:
        szLine = re.sub(' +',' ',szLine)
        lWorkingList.append(szLine.strip())
        iIndex += 1
      elif 'Error' in szLine and '\\' in szLine:
        szLine = re.sub(' +',' ',szLine)
        lWorkingList.append(szLine.strip())
        iIndex += 1
      elif 'Warning' in szLine and '\\' in szLine:
        szLine = re.sub(' +',' ',szLine)
        lWorkingList.append(szLine.strip())
        iIndex += 1
      elif '.c' in szLine or '.s' in szLine:
        iEnd = iIndex
        szLine = re.sub(' +',' ',szLine)
        lWorkingList.append(szLine.strip())
        dWorkingDict[lWorkingList[iStart]] = lWorkingList[iStart+1:iEnd]
        iStart = iIndex
        iIndex += 1
      elif 'bytes' in szLine:
        szLine = re.sub(' +',' ',szLine)
        lWorkingList.append(szLine.strip())
        iIndex += 1
      else:
        continue
#TODO error sur le .c et .s il faut s'assurer que c'est sur le dernier element donc le fichier, sinon si ya dans le path ca vap lanter
  fiInput.close()
  return dWorkingDict

def IARBuild_CheckCompilation(dParsedOutput):
  bStatus = True # Status of the compilation, TRUE if compilation was a success, FALSE  otherwise

  for key, value in dParsedOutput.items():
    for szLine in value:
      if 'Error[Pe' in szLine:
        bStatus = False
        cp.printPrefix(10, "IAR_BUILD", 3, "/!\\ Compilation failed: " + szLine[szLine.rfind('\\') + 1:])
      elif "Failed to open file" in szLine:
        bStatus = False
        cp.printPrefix(10, "IAR_BUILD", 3, "/!\\ Error with .ewp file " + szLine[szLine.rfind('\\') + 1:])

  return bStatus

def IARBuild_CheckMemory(dParsedOutput):
  memoryList = []
  dMemory = {}
  iNbList = 0
  
  for key, value in dParsedOutput.items():
    bMemCode  = False # If CODE memory has already been found for this index
    bMemData  = False # If CODE memory has already been found for this index
    bMemConst   = False # If CODE memory has already been found for this index
    lWorkingList = [0,0,0]
    for szLine in value:
      if 'bytes of CODE memory' in szLine and bMemCode == False:        
        lWorkingList[0] = (int(str(re.search(r"(\d+(?:\s+\d{3})*)", str(szLine)).group()).replace(" ", ""))) # Find first number occurence in the line
        bMemCode = True
      elif 'bytes of DATA memory' in szLine and bMemData == False:      
        lWorkingList[1] = (int(str(re.search(r"(\d+(?:\s+\d{3})*)", str(szLine)).group()).replace(" ", ""))) # Find first number occurence in the line
        bMemData = True
      elif 'bytes of CONST memory' in szLine and bMemConst == False:      
        lWorkingList[2] = (int(str(re.search(r"(\d+(?:\s+\d{3})*)", str(szLine)).group()).replace(" ", ""))) # Find first number occurence in the line
        bMemConst = True

    memoryList.append(lWorkingList)
    dMemory[key] = memoryList[iNbList]
    iNbList += 1
  return dMemory


def IARBuild_PrintMemory(dParsedOutput):
  print("\n")
  cp.printPrefix(10, "IAR_BUILD", 3, "Memory summary for each files\n")
  for key, value in dParsedOutput.items():
    if(value):
      cp.printPrefix(10, "IAR_BUILD", 3, "{}".format(key))
      cp.printPrefix(10, "IAR_BUILD", 3, "CODE MEMORY : {} bytes".format(value[0]))
      cp.printPrefix(10, "IAR_BUILD", 3, "DATA MEMORY : {} bytes".format(value[1]))
      cp.printPrefix(10, "IAR_BUILD", 3, "CONST MEMORY: {} bytes\n".format(value[2]))


def IARBuild_CheckWarning(dParsedOutput):
  lWarning    = [] # List of warnings
  warningDict   = {} # Dictionnary of warning for each files
  iNbList     = 0 # number of list
  tag       = "Warning[Pe" 
  for key, value in dParsedOutput.items():
    warningLocList = []
    for szLine in value:  
      if tag in szLine:
        # Find last '\' which corresponds to the file name
        warningLocList.append(szLine[szLine.rfind('\\') + 1:])
    lWarning.append(warningLocList)
    warningDict[key] = lWarning[iNbList]
    iNbList += 1
  return warningDict


def IARBuild_PrintWarning(dParsedOutput):
  print("\n")
  cp.printPrefix(10, "IAR_BUILD", 3, "Warnings summary\n")
  for key, value in dParsedOutput.items():
    for szLine in value:
      cp.printPrefix(10, "IAR_BUILD", 3, "{}".format(szLine))

def IARBuild_CreateReport(dParsedOutput, dMemory, lWarning, consoleLog):

  iNbError    = 0
  iNbWarning  = 0
  iCodeSize   = 0
  iDataSize   = 0
  iConstSize  = 0

  #Error
  for key, value in dParsedOutput.items():
    for szLine in value:
      if 'Error[Pe' in szLine:
        iNbError += 1;

  #Warnings
  for key, value in dParsedOutput.items():
    for szLine in value:
      if 'Warning[Pe' in szLine:
        iNbWarning += 1;

  #Code size
  for key, value in dMemory.items():
    # print(value)
    iCodeSize += value[0]
    iDataSize += value[1]
    iConstSize += value[2]

  if consoleLog == True:
    IARBuild_PrintMemory(dMemory)
    IARBuild_PrintWarning(lWarning)
    
    print("\n")
    cp.printPrefix(10, "IAR_BUILD", 3, "Project memory summary")
    cp.printPrefix(10, "IAR_BUILD", 3, "CODE MEMORY : {} bytes".format(iCodeSize))
    cp.printPrefix(10, "IAR_BUILD", 3, "DATA MEMORY : {} bytes".format(iDataSize))
    cp.printPrefix(10, "IAR_BUILD", 3, "CONST MEMORY: {} bytes\n".format(iConstSize))

    cp.printPrefix(10, "IAR_BUILD", 3, "Compilation summary")
    cp.printPrefix(10, "IAR_BUILD", 3, "Errors : {}".format(iNbError))
    cp.printPrefix(10, "IAR_BUILD", 3, "warnings : {}".format(iNbWarning))

def IARBuild_DeleteTemporaryFiles():
  try:
    os.remove(gszIarBuildLog)
  except:
    cp.printError(5, "Could not delete file {}".format(gszIarBuildLog))

def IARBuild_GetArgument():
  global gszIarBuilderPath
  global gszIarCompilerVersion
  global gszIarProjectPath
  global gszIarConfiguration
  global gszIarSkipJob
  global bIncreaseVerbosity
  global gszIarDebugLocalPath

  try:
    sParser = argparse.ArgumentParser()
    sConfig = configparser.ConfigParser()

    sParser.add_argument('-config', action='store', required=True, help='Config file')
    sParser.add_argument('--verbosity', action='store_true', help='Will increase verbosity of the console output')

    # Parsed arguments
    sParsed = sParser.parse_args()

    bIncreaseVerbosity = sParsed.verbosity

    # Ini file
    sConfig.read(sParsed.config)

    gszIarBuilderPath       = sConfig.get('IARBUILD','IAR_BUILD_COMMAND_LINE_PATH')
    gszIarCompilerVersion   = sConfig.get('IARBUILD','IAR_COMPILER_VERSION')
    gszIarProjectPath       = sConfig.get('IARBUILD','IAR_PROJECT_EWP_PATH')
    gszIarConfiguration     = sConfig.get('IARBUILD','IAR_CONFIGURATION')
    gszIarSkipJob           = sConfig.get('IARBUILD','IAR_SKIP_JOB')
    gszIarDebugLocalPath           = sConfig.get('IARBUILD','IAR_DEBUG_LOCAL_PROJECT') #must be removed if no debug used

    # Print the configuration file
    print("\n")
    cp.printPrefix(10, "IAR_BUILD", 3, "The following configuration will be used")
    cp.printPrefix(10, "IAR_BUILD", 7, "-> IAR_BUILD_COMMAND_LINE_PATH {}".format(gszIarBuilderPath))
    cp.printPrefix(10, "IAR_BUILD", 7, "-> IAR_COMPILER_VERSION        {}".format(gszIarCompilerVersion))
    cp.printPrefix(10, "IAR_BUILD", 7, "-> IAR_PROJECT_EWP_PATH        {}".format(gszIarProjectPath))
    cp.printPrefix(10, "IAR_BUILD", 7, "-> IAR_CONFIGURATION           {}".format(gszIarConfiguration))
    cp.printPrefix(10, "IAR_BUILD", 7, "-> IAR_SKIP_JOB                {}".format(gszIarSkipJob))

  except Exception as e:
    print("\n\n\n")
    cp.printError(5, "An exception occured during parsing of the configuration file\n\n\n")
    cp.printError(5, "->     {}".format(e))
    IARBuild_Exit("FAILED")

def IARBuild_InvokeCommandLine():
  try:
    print("\n")
    cp.printPrefix(10, "IAR_BUILD", 3, "Starting IAR Build command line with the following parameter")

    # Create IAR command line as follow: "<installation dir>\common\bin\IarBuild.exe" "<project dir>\projectname.ewp" -build <configuration> -log all
    szIarBuildUtiltyPath = '"' + gszIarBuilderPath + '"'
    szIarProjectPath = '"' + gszIarDebugLocalPath + '\\' + gszIarProjectPath +'"'
    #szIarProjectPath = '"' + gszInitialWorkingDir + '\\'+ gszIarProjectPath + '"' TODO REMOVE HERE WHEN PROJECT IS STORED ON GIT
    szCmdLine = szIarBuildUtiltyPath + ' ' + szIarProjectPath + ' ' + "-build" + ' ' + gszIarConfiguration + " -log all >> " + gszIarBuildLog 
    cp.printPrefix(10, "IAR_BUILD", 3, "Command line is     {}".format(szCmdLine))

    try:
      sIarProc = subprocess.Popen(szCmdLine, shell = True)
      sIarProc.communicate()
    except Exception as e:
      cp.printError(5, "->     {}".format(e))
      IARBuild_Exit("FAILED")

  except Exception as e:
    print("\n\n\n")
    cp.printError(5, "An exception occured while starting IAR Builder from the command line\n\n\n")
    cp.printError(5, "->     {}".format(e))
    IARBuild_Exit("FAILED")

def IARBuild_Exit(ret):
  if (ret == "SUCCESS"):
    sys.exit(0)
  else:
    sys.exit(1)


"""----------------------------------------------------------------------------------------------"""
"""-------------------------------------------- MAIN --------------------------------------------"""
"""----------------------------------------------------------------------------------------------"""
if __name__ == '__main__':
  try:
    print("\n\n\n\n")
    cp.printPrefix(10, "IAR_BUILD", 3, "/*---------- IARBUILD.PY START ----------*/\n\n\n\n")

    # [x] Read command line arguments
    IARBuild_GetArgument()

    # [x] Initialize script variable
    gszInitialWorkingDir = os.getcwd()
    cp.printPrefix(10, "IAR_BUILD", 3, "This script has been called from the following folder {}".format(gszInitialWorkingDir))
      
    # [x] Check if job has been skipped
    if (gszIarSkipJob != "False"):
      cp.printPrefix(10, "IAR_BUILD", 3, "This job has been skipped thanks to the ini file, should be set as UNSTABLE later")
      IARBuild_Exit("SUCCESS")
    else:
      # [x] Call IAR Build command line
      IARBuild_InvokeCommandLine()

      # [x] Parsed IAR compilation log to retrieve usefull information
      dParsedOutput = IARBuild_ParsedOutput(gszIarBuildLog)

      # [x] Create a dictionnary of the memory
      dMemory = IARBuild_CheckMemory(dParsedOutput)

      # [x] Create a list of the warnings 
      lWarning = IARBuild_CheckWarning(dParsedOutput)

      # [x] Create a report of the IAR Build command line output
      IARBuild_CreateReport(dParsedOutput, dMemory, lWarning, bIncreaseVerbosity)

      # [x] Check IAR compilation status
      bCompilationStatus = IARBuild_CheckCompilation(dParsedOutput)

      # [x] Delete temporary files
      IARBuild_DeleteTemporaryFiles()

      # [x] Quit pipeline if compilation failed
      print('\n')
      if bCompilationStatus == 0:
        cp.printPrefix(10, "IAR_BUILD", 3, "COMPILATION FAILED")
        IARBuild_Exit("FAILED")
      else:
        cp.printPrefix(10, "IAR_BUILD", 3, "COMPILATION SUCCESS")
        IARBuild_Exit("SUCCESS")

  except Exception as e:
    print("\n\n\n")
    cp.printError(5, "An exception occured in main\n\n\n")
    cp.printError(5, "->     {}".format(e))
    IARBuild_Exit("FAILED")