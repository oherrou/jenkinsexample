__author__ = "Delta Dore"
__license__ = "GPL"
__version__ = "1.0.0"
__maintainer__ = "Olivier Herrou"
__email__ = "oherrou.ext@deltadore.com"
__status__ = "Production"

"""  IAR Build libary
  This library has been developed to run doxygen on a specific folder

  Tested with the following python version:
   - Windows 7 32bits, Python V3.6.4
"""

"""----------------------------------------------------------------------------------------------"""
"""------------------------------------------- IMPORT -------------------------------------------"""
"""----------------------------------------------------------------------------------------------"""
import sys
import subprocess
import re
import os
import datetime
import argparse
import configparser

sys.path.append('python-tools\\CustomPrint')
import CustomPrint as cp



"""----------------------------------------------------------------------------------------------"""
"""------------------------------------------- GLOBAL -------------------------------------------"""
"""----------------------------------------------------------------------------------------------"""
gszInitialWorkingDir  = '' #Working directory from where this python script has been called
gszDoxygenUtilityPath = ''
gszDoxyfilePath       = ''
gszDoxyFileSkipJob    = ''

bIncreaseVerbosity = False


"""----------------------------------------------------------------------------------------------"""
"""------------------------------------------ FUNCTIONS -----------------------------------------"""
"""----------------------------------------------------------------------------------------------"""
def Doxygen_GetArgument():
  try:
    global gszDoxygenUtilityPath
    global gszDoxyfilePath
    global gszDoxyFileFile
    global gszDoxyFileSkipJob
    global bIncreaseVerbosity

    sParser = argparse.ArgumentParser()
    sConfig = configparser.ConfigParser()

    sParser.add_argument('-config', action='store', required=True, help='Config file')
    sParser.add_argument('--verbosity', action='store_true', help='Will increase verbosity of the console output')

    # Parsed arguments
    sParsed = sParser.parse_args()

    bIncreaseVerbosity = sParsed.verbosity

    # Ini file
    sConfig.read(sParsed.config)

    gszDoxygenUtilityPath = sConfig.get('DOXYGEN','DOXYGEN_COMMAND_LINE_PATH')
    gszDoxyfilePath       = sConfig.get('DOXYGEN','DOXYFILE_PATH')
    gszDoxyFileFile       = sConfig.get('DOXYGEN','DOXYFILE_FILE')
    gszDoxyFileFile       = sConfig.get('DOXYGEN','DOXYFILE_FILE')
    gszDoxyFileSkipJob    = sConfig.get('DOXYGEN','DOXYFILE_SKIP_JOB')

    # Print the configuration file
    print("\n")
    cp.printPrefix(10, "DOXYGEN", 3, "The following configuration will be used")
    cp.printPrefix(10, "DOXYGEN", 7, "-> DOXYGEN_COMMAND_LINE_PATH {}".format(gszDoxygenUtilityPath))
    cp.printPrefix(10, "DOXYGEN", 7, "-> DOXYFILE_PATH {}".format(gszDoxyfilePath))
    cp.printPrefix(10, "DOXYGEN", 7, "-> DOXYFILE_FILE {}".format(gszDoxyFileFile))
    cp.printPrefix(10, "DOXYGEN", 7, "-> DOXYFILE_FILE {}".format(gszDoxyFileFile))
    cp.printPrefix(10, "DOXYGEN", 7, "-> DOXYFILE_SKIP_JOB {}".format(gszDoxyFileSkipJob))

  except Exception as e:
    print("\n\n\n")
    cp.printError(5, "An exception occured during parsing of the configuration file\n\n\n")
    cp.printError(5, "->   {}".format(e))
    IARBuild_Exit("FAILED")

def Doxygen_InvokeCommandLine():
  try:
    cp.printPrefix(10, "DOXYGEN", 3, "Starting Doxygen command line with the following parameter")
    cp.printPrefix(10, "DOXYGEN", 3, "-> Doxygen command line utility is")
    cp.printPrefix(10, "DOXYGEN", 3, "->   {}".format(gszDoxygenUtilityPath))
    cp.printPrefix(10, "DOXYGEN", 3, "-> Doxyfile is")
    cp.printPrefix(10, "DOXYGEN", 3, "->   {}".format(gszDoxyfilePath))

    # Move to the Doxyfile folder to run the command line utility
    szDoxyfilePath = os.getcwd() + '\\' + gszDoxyfilePath
    os.chdir(szDoxyfilePath)

    # Create Doxygen line as follow: "<installation dir>\doxygen.exe" "<doxyfile path>"
    szCmdLine = '"' + gszDoxygenUtilityPath + '"' + ' ' + '"' + szDoxyfilePath + "\\" + gszDoxyFileFile + '"'
    cp.printPrefix(10, "DOXYGEN", 3, "Command line is   {}".format(szCmdLine))
     
    try:
      sDoxygenProc = subprocess.Popen(szCmdLine, shell = True)
      sDoxygenProc.communicate()
    except Exception as e:
      cp.printError(5, "->   {}".format(e))
  except Exception as e:
    print("\n\n\n")
    cp.printError(5, "An exception occured while starting Doxygen from the command line\n\n\n")
    cp.printError(5, "->   {}".format(e))
    IARBuild_Exit("FAILED")

def Doxygen_Exit(ret):
  if (ret == "SUCCESS"):
    sys.exit(0)
  else:
    sys.exit(1)


"""----------------------------------------------------------------------------------------------"""
"""-------------------------------------------- MAIN --------------------------------------------"""
"""----------------------------------------------------------------------------------------------"""
if __name__ == '__main__':
  try:
    print("\n\n\n\n")
    cp.printPrefix(10, "DOXYGEN", 3, "/*---------- DOXYGENJOB.PY START ----------*/\n\n\n\n")

    # [x] Initialize script variable
    gszInitialWorkingDir = os.getcwd()
    cp.printPrefix(10, "DOXYGEN", 3, "This script has been called from the following folder {}".format(gszInitialWorkingDir))
      
    # [x] Read command line arguments
    Doxygen_GetArgument()
  
    if (gszDoxyFileSkipJob != "False"):
      cp.printPrefix(10, "DOXYGEN", 3, "This job has been skipped thanks to the ini file, should be set as UNSTABLE later")
      Doxygen_Exit("SUCCESS")
    else:
      # [x] Call Doxygen Utility command line
      Doxygen_InvokeCommandLine()

      # [x] Doxygen log file is not analyse yet so always success, need to find condition for failure 
      Doxygen_Exit("SUCCESS")

  except Exception as e:
    print("\n\n\n")
    cp.printError(5, "An exception occured in main\n\n\n")
    cp.printError(5, "->   {}".format(e))
    IARBuild_Exit("FAILED")
